#ifndef __GLWIDGET__INCLUDE__
#define __GLWIDGET__INCLUDE__

#include <QOpenGLWidget>
#include <QOpenGLFunctions_3_3_Core>
#include <QMouseEvent>

#define GLM_FORCE_RADIANS

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

using glm::inverse;
using glm::vec2;
using glm::vec3;
using glm::vec4;
using glm::mat4;
using glm::perspective;
using glm::normalize;
using glm::length;
using glm::cross;
using glm::dot;
using glm::rotate;
using glm::value_ptr;
using glm::lookAt;

class GLWidget : public QOpenGLWidget, protected QOpenGLFunctions_3_3_Core {
	Q_OBJECT
public:
	GLWidget(QWidget *parent=0);
	~GLWidget();

protected:
	void initializeGL();
	void resizeGL(int w, int h);
	void paintGL();

	void mousePressEvent(QMouseEvent *event);
	void mouseMoveEvent(QMouseEvent *event);

private:
	GLuint loadShaders(const char* vertf, const char* fragf);
	vec3 pointOnVirtualTrackball(const vec2 &pt);

	void renderWall();
	void renderCube(float x, float y, float z, float rot);

	GLuint cubeVAO;

	GLuint program;

	mat4 projMat;
	mat4 viewMat;
	mat4 trackballMat;
	mat4 modelMat;

	int width;
	int height;
	vec3 lastVPt;

	int wallWidth;
	int wallHeight;
	vec3 brickDims;
	float brickSpacing;
	bool circleWall;

public slots:
	void setWallWidth(int w);
	void setWallHeight(int h);
	void setWallSpacing(double space);
	void setBrickX(double x);
	void setBrickY(double y);
	void setBrickZ(double z);
	void isCircleWall(bool circle);
};

#endif