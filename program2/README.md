# Procedural Modeling

I chose to use the standard brick wall as my procedural model.

![screencap](http://i.imgur.com/iH4ZCRJ.png)

## Functionality

*Implement a virtual trackball.* The virtual trackball from previous labs has been implemeneted.

*Shade your object using diffuse, specular and ambient lighting.* Phong shading was used to light the model. The light source 
moves with the camera, allowing good light at all viewing angles.

*Have four configurable options to adjust your model.* The four configurable options are wall dimensions (width and height), 
spacing between bricks, the dimensions of the bricks, and an option between a normal wall and a circular wall.

*Demonstrate creativity and self expression.* I chose to implement controls with a Qt GUI. All parameters of all four options are available using the GUI.
