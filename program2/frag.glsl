#version 330

in vec3 fpos;
in vec3 fnormal;

out vec4 color_out;

uniform vec3 diffColor;
uniform vec3 ambColor;
uniform vec3 lightPos;
uniform mat4 view;
uniform mat4 model;

void main() {
	vec3 P = fpos;
	vec3 N = normalize(fnormal);
	vec3 light = (view * vec4(lightPos, 1)).xyz;
	vec3 L = normalize(light - P);

	vec3 V = normalize(-P);
	vec3 R = 2*dot(L, N)*N - L;

	float diffuse = 1.0*max(0, dot(N, L));
	float specular = 0.5*max(0, pow(dot(R, V), 40));
	float amb = 0.1;

	color_out = vec4(diffuse*diffColor + specular + amb*ambColor, 1);
}