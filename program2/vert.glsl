#version 330

uniform mat4 projection;
uniform mat4 view;
uniform mat4 model;

in vec3 position;
in vec3 normal;

out vec3 fpos;
out vec3 fnormal;

void main() {
	vec4 pos = view * model * vec4(position, 1); // position in eye space
	gl_Position = projection * pos;
	fpos = pos.xyz;
	fnormal = normalize((transpose(inverse(view * model)) * vec4(normal, 0)).xyz);
}