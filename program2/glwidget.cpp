#include "glwidget.h"

#include <iostream>

#include <QTextStream>

#ifndef M_PI
    #define M_PI 3.14159265358979323846
#endif

using namespace std;

GLWidget::GLWidget(QWidget *parent) : QOpenGLWidget(parent) {
	wallWidth = 6;
	wallHeight = 4;
	brickDims = vec3(1.8, 0.9, 0.9);
	brickSpacing = 0.1;
	circleWall = false;
}
GLWidget::~GLWidget(){}

void GLWidget::initializeGL(){
	initializeOpenGLFunctions();

	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
	glEnable(GL_DEPTH_TEST);

	program = loadShaders(":/vert.glsl", ":/frag.glsl");

	float pos[] = {
			 0.5f,  0.5f,  0.5f, // 0
			 0.5f, -0.5f,  0.5f, // 2
			 0.5f,  0.5f, -0.5f, // 1

			 0.5f,  0.5f, -0.5f, // 1
			 0.5f, -0.5f,  0.5f, // 2
			 0.5f, -0.5f, -0.5f, // 3

			 0.5f,  0.5f,  0.5f, // 0
			-0.5f,  0.5f,  0.5f, // 4
			 0.5f,  0.5f, -0.5f, // 1

			 0.5f,  0.5f, -0.5f, // 1
			-0.5f,  0.5f,  0.5f, // 4
			-0.5f,  0.5f, -0.5f, // 5

			 0.5f,  0.5f,  0.5f, // 0
			-0.5f,  0.5f,  0.5f, // 4
			 0.5f, -0.5f,  0.5f, // 2

			 0.5f, -0.5f,  0.5f, // 2
			-0.5f,  0.5f,  0.5f, // 4
			-0.5f, -0.5f,  0.5f, // 6 .

			-0.5f,  0.5f,  0.5f, // 4
			-0.5f, -0.5f,  0.5f, // 6
			-0.5f,  0.5f, -0.5f, // 5

			-0.5f,  0.5f, -0.5f, // 5
			-0.5f, -0.5f,  0.5f, // 6
			-0.5f, -0.5f, -0.5f, // 7 . 

			 0.5f, -0.5f,  0.5f, // 2
			-0.5f, -0.5f,  0.5f, // 6
			 0.5f, -0.5f, -0.5f, // 3

			 0.5f, -0.5f, -0.5f, // 3
			-0.5f, -0.5f,  0.5f, // 6
			-0.5f, -0.5f, -0.5f, // 7 . 

			 0.5f,  0.5f, -0.5f, // 1
			-0.5f,  0.5f, -0.5f, // 5
			 0.5f, -0.5f, -0.5f, // 3

			 0.5f, -0.5f, -0.5f, // 3
			-0.5f,  0.5f, -0.5f, // 5
			-0.5f, -0.5f, -0.5f, // 7
	};

	float norm[] = {
			1, 0, 0,
			1, 0, 0,
			1, 0, 0,
			1, 0, 0,
			1, 0, 0,
			1, 0, 0,

			0, 1, 0,
			0, 1, 0,
			0, 1, 0,
			0, 1, 0,
			0, 1, 0,
			0, 1, 0,

			0, 0, 1,
			0, 0, 1,
			0, 0, 1,
			0, 0, 1,
			0, 0, 1,
			0, 0, 1,

			-1, 0, 0,
			-1, 0, 0,
			-1, 0, 0,
			-1, 0, 0,
			-1, 0, 0,
			-1, 0, 0,

			0, -1, 0,
			0, -1, 0,
			0, -1, 0,
			0, -1, 0,
			0, -1, 0,
			0, -1, 0,

			0, 0, -1,
			0, 0, -1,
			0, 0, -1,
			0, 0, -1,
			0, 0, -1,
			0, 0, -1
	};

	glGenVertexArrays(1, &cubeVAO);
	glBindVertexArray(cubeVAO);

	GLuint posBuffer;
	glGenBuffers(1, &posBuffer);
	glBindBuffer(GL_ARRAY_BUFFER, posBuffer);
	glBufferData(GL_ARRAY_BUFFER, sizeof(pos), pos, GL_STATIC_DRAW);
	GLint posIndex = glGetAttribLocation(program, "position");
	glEnableVertexAttribArray(posIndex);
	glVertexAttribPointer(posIndex, 3, GL_FLOAT, GL_FALSE, 0, 0);

	GLuint normBuffer;
	glGenBuffers(1, &normBuffer);
	glBindBuffer(GL_ARRAY_BUFFER, normBuffer);
	glBufferData(GL_ARRAY_BUFFER, sizeof(norm), norm, GL_STATIC_DRAW);
	GLint normIndex = glGetAttribLocation(program, "normal");
	glEnableVertexAttribArray(normIndex);
	glVertexAttribPointer(normIndex, 3, GL_FLOAT, GL_FALSE, 0, 0);

	//projMat = perspective(45.0f, 1.0f, 1.0f, 100.0f);
	//viewMat = lookAt(vec3(0, 0, 10),vec3(0, 0, 0),vec3(0, 1, 0));
}

void GLWidget::resizeGL(int w, int h){
	glViewport(0,0,w,h);

	width = w;
	height = h;

	float aspect =  (1.0f * w)/h;

	projMat = perspective(45.0f, aspect, 1.0f, 100.0f);
	viewMat = lookAt(vec3(0, 0, -15),vec3(0,0,0),vec3(0,1,0));
}

void GLWidget::paintGL(){
	glClear(GL_COLOR_BUFFER_BIT);

	glUseProgram(program);
	glBindVertexArray(cubeVAO);

	GLint diffUniform = glGetUniformLocation(program, "diffColor");
	glUniform3f(diffUniform, 0.75f, 0.17f, 0.11f);

	GLint ambUniform = glGetUniformLocation(program, "ambColor");
	glUniform3f(ambUniform, 1, 0.2f, 0.2f);

	GLint lightUniform = glGetUniformLocation(program, "lightPos");
	glUniform3f(lightUniform, 10, 15, -20);

	GLint projUniform = glGetUniformLocation(program, "projection");
	glUniformMatrix4fv(projUniform, 1, false, value_ptr(projMat));

	GLint viewUniform = glGetUniformLocation(program, "view");
	glUniformMatrix4fv(viewUniform, 1, false, value_ptr(viewMat));

	for(int y = 0; y < wallHeight; y++){
		float yf = y*(brickDims.y + brickSpacing);
		if(circleWall){
			float r = (1.1 * wallWidth * (brickDims.x + brickSpacing))/(2*M_PI);
			for(float t = 0; t < (2*M_PI); t += (2*M_PI)/wallWidth){
				float th = t;
				if(y % 2 == 0)
					th += M_PI/wallWidth;
				float xf = r*sin(th);
				float zf = r*cos(th);
				renderCube(xf, yf, zf, th);
			}
		}else{
			for(int x = 0; x < wallWidth; x++){
				float xf;
				if(y % 2 == 0)
					xf = x*(brickDims.x + brickSpacing) - (wallWidth*(brickDims.x + brickSpacing) - brickSpacing)/2;
				else
					xf = (x+0.5f)*(brickDims.x + brickSpacing) - (wallWidth*(brickDims.x + brickSpacing) - brickSpacing)/2;
				renderCube(xf, yf, 0, 0);
			}
		}
	}
}

void GLWidget::renderCube(float x, float y, float z, float rot){
	modelMat = trackballMat * glm::translate(mat4(1.0f), vec3(x, y, z)) * glm::rotate(mat4(1.0f), rot, vec3(0, 1, 0)) * glm::scale(mat4(1.0f), brickDims);

	GLint modelUniform = glGetUniformLocation(program, "model");
	glUniformMatrix4fv(modelUniform, 1, false, value_ptr(modelMat));

	glDrawArrays(GL_TRIANGLES, 0, 36);
}

GLuint GLWidget::loadShaders(const char* vertf, const char* fragf) {
	GLuint program = glCreateProgram();

	// read vertex shader from Qt resource file
	QFile vertFile(vertf);
	vertFile.open(QFile::ReadOnly | QFile::Text);
	QString vertString;
	QTextStream vertStream(&vertFile);
	vertString.append(vertStream.readAll());
	std::string vertSTLString = vertString.toStdString();

	const GLchar* vertSource = vertSTLString.c_str();

	GLuint vertShader = glCreateShader(GL_VERTEX_SHADER);
	glShaderSource(vertShader, 1, &vertSource, NULL);
	glCompileShader(vertShader);
	{
		GLint compiled;
		glGetShaderiv( vertShader, GL_COMPILE_STATUS, &compiled );
		if ( !compiled ) {
			GLsizei len;
			glGetShaderiv( vertShader, GL_INFO_LOG_LENGTH, &len );

			GLchar* log = new GLchar[len+1];
			glGetShaderInfoLog( vertShader, len, &len, log );
			std::cout << "Shader compilation failed: " << log << std::endl;
			delete [] log;
		}
	}
	glAttachShader(program, vertShader);

	// read fragment shader from Qt resource file
	QFile fragFile(fragf);
	fragFile.open(QFile::ReadOnly | QFile::Text);
	QString fragString;
	QTextStream fragStream(&fragFile);
	fragString.append(fragStream.readAll());
	std::string fragSTLString = fragString.toStdString();

	const GLchar* fragSource = fragSTLString.c_str();

	GLuint fragShader = glCreateShader(GL_FRAGMENT_SHADER);
	glShaderSource(fragShader, 1, &fragSource, NULL);
	glCompileShader(fragShader);
	{
		GLint compiled;
		glGetShaderiv( fragShader, GL_COMPILE_STATUS, &compiled );
		if ( !compiled ) {
			GLsizei len;
			glGetShaderiv( fragShader, GL_INFO_LOG_LENGTH, &len );

			GLchar* log = new GLchar[len+1];
			glGetShaderInfoLog( fragShader, len, &len, log );
			std::cerr << "Shader compilation failed: " << log << std::endl;
			delete [] log;
		}
	}
	glAttachShader(program, fragShader);

	glLinkProgram(program);
	{
		GLint linked;
		glGetProgramiv( program, GL_LINK_STATUS, &linked );
		if ( !linked ) {
			GLsizei len;
			glGetProgramiv( program, GL_INFO_LOG_LENGTH, &len );

			GLchar* log = new GLchar[len+1];
			glGetProgramInfoLog( program, len, &len, log );
			std::cout << "Shader linker failed: " << log << std::endl;
			delete [] log;
		}
	}

	return program;
}

void GLWidget::mousePressEvent(QMouseEvent *event) {
	vec2 pt(event->x(), event->y());
	lastVPt = normalize(pointOnVirtualTrackball(pt));
}

void GLWidget::mouseMoveEvent(QMouseEvent *event) {
	vec2 pt(event->x(), event->y());
	vec3 vPt = normalize(pointOnVirtualTrackball(pt));

	vec3 axis = cross(lastVPt, vPt);
	if(length(axis) >= .00001) {
		axis = normalize(axis);
		float angle = acos(dot(vPt,lastVPt));
		mat4 r = rotate(mat4(1.0f), angle, axis);

		trackballMat = r*trackballMat;

		/*glUseProgram(program);
		GLint trackballUniform = glGetUniformLocation(program, "trackball");
		glUniformMatrix4fv(trackballUniform, 1, false, value_ptr(trackballMat));*/

	}
	lastVPt = vPt;
	update();
}

vec3 GLWidget::pointOnVirtualTrackball(const vec2 &pt) {
    float r = .5f;
    float rr = r*r;
    vec3 p;
    p.x = -1 + pt.x*(2.0f/width);
    p.y = -(float)height/width*(1-pt.y*(2.0f/height));

    float xx = p.x*p.x;
    float yy = p.y*p.y;

    if(xx+yy <= rr*.5) {
        p.z = sqrt(rr-(xx+yy));
    } else {
        p.z = rr*.5/sqrt(xx+yy);
    }

    return p;
}

void GLWidget::setWallWidth(int w){
	wallWidth = w;
	update();
}

void GLWidget::setWallHeight(int h){
	wallHeight = h;
	update();
}

void GLWidget::setWallSpacing(double space){
	brickSpacing = space;
	update();
}

void GLWidget::setBrickX(double x){
	brickDims.x = x;
	update();
}

void GLWidget::setBrickY(double y){
	brickDims.y = y;
	update();
}

void GLWidget::setBrickZ(double z){
	brickDims.z = z;
	update();
}

void GLWidget::isCircleWall(bool circle){
	circleWall = circle;
	update();
}