#include <QApplication>
#include <iostream>
#include <string>

#include "ui_mainwindow.h"
#include "glwidget.h"

using namespace std;

int main(int argc, char** argv) {
	QApplication a(argc, argv);

	QSurfaceFormat format;
	format.setVersion(3,3);
	format.setProfile(QSurfaceFormat::CoreProfile);
	format.setSamples(4);
	QSurfaceFormat::setDefaultFormat(format);

	QMainWindow *window = new QMainWindow;
	Ui::MainWindow ui;
	ui.setupUi(window);

	window->show();

	return a.exec();
}