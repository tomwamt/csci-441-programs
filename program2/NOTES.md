Program 2
---------

Grade Breakdown
---------------

    Virtual trackball      - 20/20
    Phong shading          - 20/20
    Option 1 (brick size)  - 10/10
    Option 2 (wall size)   - 10/10
    Option 3 (mortar size) - 10/10
    Option 4 (wall shape)  - 10/10
    General                - 14/20
    -------------------------------
                             94/100

Notes
-----
1. GUI +4
