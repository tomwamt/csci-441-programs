Program 3
---------

Grade Breakdown
---------------

    First Person Controls  - 20/20
    Animation              - 20/20
    Materials              - 20/20
    Lighting               - 10/10
    World Objects          - 10/10
    General                - 18/20
    -------------------------------
                            98/100

    Particles Extra Credit - 0/0
    -------------------------------
                            98/100

Notes
-----
1. Textures +2
2. Mouse locks to the window like a real fp game +2
3. Methods to help build a level +2
4. Built first level of Mario (awesome!) +2
