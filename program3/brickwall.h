#ifndef __BRICKWALL__INCLUDE__
#define __BRICKWALL__INCLUDE__

#include <QOpenGLFunctions_3_3_Core>

#define GLM_FORCE_RADIANS

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

using glm::vec3;
using glm::mat4;
using glm::value_ptr;

class BrickWall{
public:
	BrickWall(vec3 position, float rotation, int width, int height, vec3 dims, float spacing);
	~BrickWall();

	void render(QOpenGLFunctions_3_3_Core* gl, GLuint cubeVAO, GLuint program);

	vec3 position;
	float rotation;
	int width;
	int height;
	vec3 dims;
	float spacing;
};

#endif