#version 330

in vec3 fpos;
in vec3 fnormal;
in vec2 fuv;

out vec4 color_out;

uniform vec3 diffColor;
uniform float diffIntensity;
uniform float specIntensity;
uniform vec3 lightPos;
uniform mat4 view;
uniform bool textured;
uniform sampler2D tex;

void main() {
	vec3 P = fpos;
	vec3 N = normalize(fnormal);
	vec3 light = (view * vec4(lightPos, 1)).xyz;
	vec3 L = normalize(light - P);

	vec3 V = normalize(-P);
	vec3 R = 2*dot(L, N)*N - L;

	float diffuse = diffIntensity*max(0, dot(N, L));
	float specular = specIntensity*max(0, pow(dot(R, V), 40));
	float amb = 0.1;

	vec3 color;
	if(textured){
		color = texture(tex, fuv).rgb;
	}else{
		color = diffColor;
	}

	color_out = vec4(diffuse*color + specular + amb*color, 1);
}