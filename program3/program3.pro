HEADERS += glwidget.h brickwall.h
SOURCES += glwidget.cpp main.cpp brickwall.cpp ../include/tinyobjloader/tiny_obj_loader.cc

QT += opengl designer
CONFIG -= app_bundle
CONFIG += console c++11
INCLUDEPATH += "../include"
INCLUDEPATH += $$PWD

QMAKE_CXXFLAGS += -DGL_GLEXT_PROTOTYPES

RESOURCES += shaders.qrc textures.qrc
