#include "glwidget.h"

#include <iostream>

#include <QTextStream>

#ifndef M_PI
	#define M_PI 3.14159265358979323846
#endif

using namespace std;

GLWidget::GLWidget(QWidget *parent) : QOpenGLWidget(parent) {
	pitch = 0;
	yaw = 0;

	forward = false;
	back = false;
	left = false;
	right = false;
	up = false;
	down = false;
	flyMode = true;

	position = vec3(0, 3, 10);

	connect(&timer, &QTimer::timeout, this, &GLWidget::animate);
	timer.start(16); // 16 ms

	goombaTime = 0;
	goombaOffset = vec3(0, 0, 0);
}
GLWidget::~GLWidget(){}

void GLWidget::initializeGL(){
	initializeOpenGLFunctions();

	glClearColor(0.57f, 0.56f, 1.0f, 0.0f);
	glEnable(GL_DEPTH_TEST);

	program = loadShaders(":/vert.glsl", ":/frag.glsl");

	questionTex = loadTexture(":/question.png");
	hardTex = loadTexture(":/hard.png");
	floorTex = loadTexture(":/floor.png");

	goombaModel = loadOBJ(":/Goomba.obj");
	goombaModel.texture = loadTexture(":/goombaTex.png");

	pipeModel = loadOBJ(":/pipe.obj");
	pipeModel.texture = loadTexture(":/pipeTex.png");

	float pos[] = {
			 0.5f,  0.5f,  0.5f, // 0
			 0.5f, -0.5f,  0.5f, // 2
			 0.5f,  0.5f, -0.5f, // 1

			 0.5f,  0.5f, -0.5f, // 1
			 0.5f, -0.5f,  0.5f, // 2
			 0.5f, -0.5f, -0.5f, // 3

			 0.5f,  0.5f,  0.5f, // 0
			-0.5f,  0.5f,  0.5f, // 4
			 0.5f,  0.5f, -0.5f, // 1

			 0.5f,  0.5f, -0.5f, // 1
			-0.5f,  0.5f,  0.5f, // 4
			-0.5f,  0.5f, -0.5f, // 5

			 0.5f,  0.5f,  0.5f, // 0
			-0.5f,  0.5f,  0.5f, // 4
			 0.5f, -0.5f,  0.5f, // 2

			 0.5f, -0.5f,  0.5f, // 2
			-0.5f,  0.5f,  0.5f, // 4
			-0.5f, -0.5f,  0.5f, // 6 .

			-0.5f,  0.5f,  0.5f, // 4
			-0.5f, -0.5f,  0.5f, // 6
			-0.5f,  0.5f, -0.5f, // 5

			-0.5f,  0.5f, -0.5f, // 5
			-0.5f, -0.5f,  0.5f, // 6
			-0.5f, -0.5f, -0.5f, // 7 . 

			 0.5f, -0.5f,  0.5f, // 2
			-0.5f, -0.5f,  0.5f, // 6
			 0.5f, -0.5f, -0.5f, // 3

			 0.5f, -0.5f, -0.5f, // 3
			-0.5f, -0.5f,  0.5f, // 6
			-0.5f, -0.5f, -0.5f, // 7 . 

			 0.5f,  0.5f, -0.5f, // 1
			-0.5f,  0.5f, -0.5f, // 5
			 0.5f, -0.5f, -0.5f, // 3

			 0.5f, -0.5f, -0.5f, // 3
			-0.5f,  0.5f, -0.5f, // 5
			-0.5f, -0.5f, -0.5f, // 7
	};

	float norm[] = {
			1, 0, 0,
			1, 0, 0,
			1, 0, 0,
			1, 0, 0,
			1, 0, 0,
			1, 0, 0,

			0, 1, 0,
			0, 1, 0,
			0, 1, 0,
			0, 1, 0,
			0, 1, 0,
			0, 1, 0,

			0, 0, 1,
			0, 0, 1,
			0, 0, 1,
			0, 0, 1,
			0, 0, 1,
			0, 0, 1,

			-1, 0, 0,
			-1, 0, 0,
			-1, 0, 0,
			-1, 0, 0,
			-1, 0, 0,
			-1, 0, 0,

			0, -1, 0,
			0, -1, 0,
			0, -1, 0,
			0, -1, 0,
			0, -1, 0,
			0, -1, 0,

			0, 0, -1,
			0, 0, -1,
			0, 0, -1,
			0, 0, -1,
			0, 0, -1,
			0, 0, -1
	};

	float uv[] = {
			0, 0,
			0, 1,
			1, 0,
			1, 0,
			0, 1,
			1, 1,

			0, 0,
			1, 0,
			0, 1,
			0, 1,
			1, 0,
			1, 1,

			1, 0,
			0, 0,
			1, 1,
			1, 1,
			0, 0,
			0, 1,

			1, 0,
			1, 1,
			0, 0,
			0, 0,
			1, 1,
			0, 1,

			1, 0,
			0, 0,
			1, 1,
			1, 1,
			0, 0,
			0, 1,

			0, 0,
			1, 0,
			0, 1,
			0, 1,
			1, 0,
			1, 1,
	};

	glGenVertexArrays(1, &cubeVAO);
	glBindVertexArray(cubeVAO);

	GLuint posBuffer;
	glGenBuffers(1, &posBuffer);
	glBindBuffer(GL_ARRAY_BUFFER, posBuffer);
	glBufferData(GL_ARRAY_BUFFER, sizeof(pos), pos, GL_STATIC_DRAW);
	GLint posIndex = glGetAttribLocation(program, "position");
	glEnableVertexAttribArray(posIndex);
	glVertexAttribPointer(posIndex, 3, GL_FLOAT, GL_FALSE, 0, 0);

	GLuint normBuffer;
	glGenBuffers(1, &normBuffer);
	glBindBuffer(GL_ARRAY_BUFFER, normBuffer);
	glBufferData(GL_ARRAY_BUFFER, sizeof(norm), norm, GL_STATIC_DRAW);
	GLint normIndex = glGetAttribLocation(program, "normal");
	glEnableVertexAttribArray(normIndex);
	glVertexAttribPointer(normIndex, 3, GL_FLOAT, GL_FALSE, 0, 0);

	GLuint uvBuffer;
	glGenBuffers(1, &uvBuffer);
	glBindBuffer(GL_ARRAY_BUFFER, uvBuffer);
	glBufferData(GL_ARRAY_BUFFER, sizeof(uv), uv, GL_STATIC_DRAW);
	GLint uvIndex = glGetAttribLocation(program, "uv");
	glEnableVertexAttribArray(uvIndex);
	glVertexAttribPointer(uvIndex, 2, GL_FLOAT, GL_FALSE, 0, 0);

	float planePos[] = {
		-1, 0, -1,
		1, 0, -1,
		-1, 0, 1,
		-1, 0, 1,
		1, 0, -1,
		1, 0, 1
	};

	float planeNorm[] = {
		0, 1, 0,
		0, 1, 0,
		0, 1, 0,
		0, 1, 0,
		0, 1, 0,
		0, 1, 0
	};

	float planeUV[] = {
		0, 100,
		500, 100,
		0, 0,
		0, 0,
		500, 100,
		500, 0
	};

	glGenVertexArrays(1, &planeVAO);
	glBindVertexArray(planeVAO);

	GLuint planePosBuffer;
	glGenBuffers(1, &planePosBuffer);
	glBindBuffer(GL_ARRAY_BUFFER, planePosBuffer);
	glBufferData(GL_ARRAY_BUFFER, sizeof(planePos), planePos, GL_STATIC_DRAW);
	glEnableVertexAttribArray(posIndex);
	glVertexAttribPointer(posIndex, 3, GL_FLOAT, GL_FALSE, 0, 0);

	GLuint planeNormBuffer;
	glGenBuffers(1, &planeNormBuffer);
	glBindBuffer(GL_ARRAY_BUFFER, planeNormBuffer);
	glBufferData(GL_ARRAY_BUFFER, sizeof(planeNorm), planeNorm, GL_STATIC_DRAW);
	glEnableVertexAttribArray(normIndex);
	glVertexAttribPointer(normIndex, 3, GL_FLOAT, GL_FALSE, 0, 0);

	GLuint planeUvBuffer;
	glGenBuffers(1, &planeUvBuffer);
	glBindBuffer(GL_ARRAY_BUFFER, planeUvBuffer);
	glBufferData(GL_ARRAY_BUFFER, sizeof(planeUV), planeUV, GL_STATIC_DRAW);
	glEnableVertexAttribArray(uvIndex);
	glVertexAttribPointer(uvIndex, 2, GL_FLOAT, GL_FALSE, 0, 0);

	center = vec2(100,100);

	placeQuestionBlock(0, 4);

	placeBrickBlock(4, 4);
	placeQuestionBlock(5, 4);
	placeBrickBlock(6, 4);
	placeQuestionBlock(7, 4);
	placeBrickBlock(8, 4);

	placeQuestionBlock(6, 8);

	placeGoomba(6, 1);

	placePipe(12, 2);
	placePipe(22, 3);
	placeGoomba(24, 1);
	placePipe(30, 4);
	placeGoomba(35, 1);
	placeGoomba(36, 1);
	placePipe(41, 4); // A

	placeHole(53);

	placeBrickBlock(61, 4);
	placeQuestionBlock(62, 4);
	placeBrickBlock(63, 4);

	placeBrickBlock(64, 8);
	placeBrickBlock(65, 8);
	placeGoomba(65, 9);
	placeBrickBlock(66, 8);
	placeBrickBlock(67, 8);
	placeGoomba(67, 9);
	placeBrickBlock(68, 8);
	placeBrickBlock(69, 8);
	placeBrickBlock(70, 8);
	placeBrickBlock(71, 8);

	placeHole(70);

	placeBrickBlock(75, 8);
	placeBrickBlock(76, 8);
	placeBrickBlock(77, 8);
	placeQuestionBlock(78, 8);

	placeBrickBlock(78, 4);

	placeGoomba(81, 1);
	placeGoomba(82, 1);

	placeBrickBlock(84, 4);
	placeBrickBlock(85, 4); // Star

	placeQuestionBlock(90, 4);
	placeQuestionBlock(93, 4);
	placeQuestionBlock(93, 8);
	placeQuestionBlock(96, 4);

	placeGoomba(98, 1);
	placeGoomba(99, 1);

	placeBrickBlock(102, 4);

	placeBrickBlock(105, 8);
	placeBrickBlock(106, 8);
	placeBrickBlock(107, 8);

	placeGoomba(108, 1);
	placeGoomba(109, 1);
	placeGoomba(112, 1);
	placeGoomba(113, 1);

	placeBrickBlock(112, 8);
	placeQuestionBlock(113, 8);
	placeQuestionBlock(114, 8);
	placeBrickBlock(115, 8);

	placeBrickBlock(113, 4);
	placeBrickBlock(114, 4);

	placeHardBlock(118, 1); // First staircase
	placeHardBlock(119, 1);
	placeHardBlock(119, 2);
	placeHardBlock(120, 1);
	placeHardBlock(120, 2);
	placeHardBlock(120, 3);
	placeHardBlock(121, 1);
	placeHardBlock(121, 2);
	placeHardBlock(121, 3);
	placeHardBlock(121, 4);

	placeHardBlock(124, 1);
	placeHardBlock(124, 2);
	placeHardBlock(124, 3);
	placeHardBlock(124, 4);
	placeHardBlock(125, 1);
	placeHardBlock(125, 2);
	placeHardBlock(125, 3);
	placeHardBlock(126, 1);
	placeHardBlock(126, 2);
	placeHardBlock(127, 1);

	placeHardBlock(132, 1);
	placeHardBlock(133, 1);
	placeHardBlock(133, 2);
	placeHardBlock(134, 1);
	placeHardBlock(134, 2);
	placeHardBlock(134, 3);
	placeHardBlock(135, 1);
	placeHardBlock(135, 2);
	placeHardBlock(135, 3);
	placeHardBlock(135, 4);
	placeHardBlock(136, 1);
	placeHardBlock(136, 2);
	placeHardBlock(136, 3);
	placeHardBlock(136, 4);

	placeHole(137);

	placeHardBlock(139, 1);
	placeHardBlock(139, 2);
	placeHardBlock(139, 3);
	placeHardBlock(139, 4);
	placeHardBlock(140, 1);
	placeHardBlock(140, 2);
	placeHardBlock(140, 3);
	placeHardBlock(141, 1);
	placeHardBlock(141, 2);
	placeHardBlock(142, 1);

	placePipe(147, 2);

	placeBrickBlock(152, 4);
	placeBrickBlock(153, 4);
	placeQuestionBlock(154, 4);
	placeBrickBlock(155, 4);

	placeGoomba(158, 1);
	placeGoomba(159, 1);

	placePipe(163, 2);

	placeHardBlock(165, 1); // Last staircase
	placeHardBlock(166, 1);
	placeHardBlock(166, 2);
	placeHardBlock(167, 1);
	placeHardBlock(167, 2);
	placeHardBlock(167, 3);
	placeHardBlock(168, 1);
	placeHardBlock(168, 2);
	placeHardBlock(168, 3);
	placeHardBlock(168, 4);
	placeHardBlock(169, 1);
	placeHardBlock(169, 2);
	placeHardBlock(169, 3);
	placeHardBlock(169, 4);
	placeHardBlock(169, 5);
	placeHardBlock(170, 1);
	placeHardBlock(170, 2);
	placeHardBlock(170, 3);
	placeHardBlock(170, 4);
	placeHardBlock(170, 5);
	placeHardBlock(170, 6);
	placeHardBlock(171, 1);
	placeHardBlock(171, 2);
	placeHardBlock(171, 3);
	placeHardBlock(171, 4);
	placeHardBlock(171, 5);
	placeHardBlock(171, 6);
	placeHardBlock(171, 7);
	placeHardBlock(172, 1);
	placeHardBlock(172, 2);
	placeHardBlock(172, 3);
	placeHardBlock(172, 4);
	placeHardBlock(172, 5);
	placeHardBlock(172, 6);
	placeHardBlock(172, 7);
	placeHardBlock(172, 8);
	placeHardBlock(173, 1);
	placeHardBlock(173, 2);
	placeHardBlock(173, 3);
	placeHardBlock(173, 4);
	placeHardBlock(173, 5);
	placeHardBlock(173, 6);
	placeHardBlock(173, 7);
	placeHardBlock(173, 8);
}

void GLWidget::placeBrickBlock(int x, int y){
	vec3 dims = vec3(0.38, 0.23, 0.18);
	BrickWall  wall1(vec3(x+0.09, y, -0.4), 0, 2, 4, dims, 0.02);
	BrickWall  wall3(vec3(x+0.4, y, 0.09), -M_PI/2, 2, 4, dims, 0.02);
	BrickWall  wall2(vec3(x-0.09, y, 0.4), M_PI, 2, 4, dims, 0.02);
	BrickWall  wall4(vec3(x-0.4, y, -0.09), M_PI/2, 2, 4, dims, 0.02);
	BrickWall    top(vec3(x+0.3, y+0.75, 0), 0, 1, 1, vec3(0.59, 0.23, 0.59), 0);
	BrickWall bottom(vec3(x+0.3, y, 0), 0, 1, 1, vec3(0.59, 0.23, 0.59), 0);
	walls.push_back(wall1);
	walls.push_back(wall2);
	walls.push_back(wall3);
	walls.push_back(wall4);
	walls.push_back(top);
	walls.push_back(bottom);
}

void GLWidget::placeQuestionBlock(int x, int y){
	question.push_back(vec2(x, y+0.385));
}

void GLWidget::placeHardBlock(int x, int y){
	hardBlocks.push_back(vec2(x, y+0.385));
}

void GLWidget::placeGoomba(int x, int y){
	goombas.push_back(vec3(x, y-0.1, 0));
}

void GLWidget::placePipe(int x, int height){
	pipes.push_back(vec2(x, height));
}

void GLWidget::placeHole(int x){
	holes.push_back(x+0.5f);
}

void GLWidget::moveEvent(QMoveEvent* event){
	QPoint p = mapToGlobal(QPoint(width()/2, height()/2));
	center.x = p.x();
	center.y = p.y();
}

void GLWidget::resizeGL(int w, int h){
	glViewport(0,0,w,h);

	//width = w;
	//height = h;

	float aspect =  (1.0f * w)/h;

	projMat = perspective(45.0f, aspect, 0.1f, 200.0f);

	QPoint p = mapToGlobal(QPoint(w/2, h/2));
	center.x = p.x();
	center.y = p.y();
}

void GLWidget::paintGL(){
	glClear(GL_COLOR_BUFFER_BIT  | GL_DEPTH_BUFFER_BIT);

	glUseProgram(program);
	glBindVertexArray(cubeVAO);

	GLint modelUniform = glGetUniformLocation(program, "model");
	mat4 modelMat;

	GLint lightUniform = glGetUniformLocation(program, "lightPos");
	glUniform3f(lightUniform, 100, 50, 40);

	GLint projUniform = glGetUniformLocation(program, "projection");
	glUniformMatrix4fv(projUniform, 1, false, value_ptr(projMat));

	GLint viewUniform = glGetUniformLocation(program, "view");
	glUniformMatrix4fv(viewUniform, 1, false, value_ptr(viewMat));

	GLint diffUniform = glGetUniformLocation(program, "diffColor");
	glUniform3f(diffUniform, 0.6f, 0.3f, 0);

	GLint diffIntUniform = glGetUniformLocation(program, "diffIntensity");
	glUniform1f(diffIntUniform, 1.0f);

	GLint specIntUniform = glGetUniformLocation(program, "specIntensity");
	glUniform1f(specIntUniform, 0.2f);

	GLint texUniform = glGetUniformLocation(program, "textured");
	glUniform1i(texUniform, 0);

	for(BrickWall wall : walls){
		wall.render(this, cubeVAO, program);
	}

	// Pipes
	glBindVertexArray(pipeModel.vao);
	glUniform3f(diffUniform, 0, 0.6f, 0);
	glUniform1f(specIntUniform, 0.6f);
	for(vec2 pvec : pipes){
		modelMat = glm::translate(mat4(1.0f), vec3(pvec, 0)) * glm::scale(mat4(1.0f), vec3(0.12f));
		glUniformMatrix4fv(modelUniform, 1, false, value_ptr(modelMat));
		glDrawElements(GL_TRIANGLES, pipeModel.numIndices, GL_UNSIGNED_INT, 0);
	}

	// Blocks
	glBindVertexArray(cubeVAO);
	glUniform1i(texUniform, 1);
	glUniform1f(specIntUniform, 0);
	glActiveTexture(GL_TEXTURE0);
	GLint texSamplerUniform = glGetUniformLocation(program, "tex");
	glUniform1i(texSamplerUniform, 0);
	glBindTexture(GL_TEXTURE_2D, questionTex);

	for(vec2 qvec : question){
		modelMat = glm::translate(mat4(1.0f), vec3(qvec, 0));
		glUniformMatrix4fv(modelUniform, 1, false, value_ptr(modelMat));
		glDrawArrays(GL_TRIANGLES, 0, 36);
	}

	glBindTexture(GL_TEXTURE_2D, hardTex);

	for(vec2 hvec : hardBlocks){
		modelMat = glm::translate(mat4(1.0f), vec3(hvec, 0));
		glUniformMatrix4fv(modelUniform, 1, false, value_ptr(modelMat));
		glDrawArrays(GL_TRIANGLES, 0, 36);
	}

	// Goombas
	glBindVertexArray(goombaModel.vao);
	glBindTexture(GL_TEXTURE_2D, goombaModel.texture);
	glUniform1f(specIntUniform, 0.1f);
	glUniform1i(texUniform, 0);
	glUniform3f(diffUniform, 0.6f, 0.3f, 0);
	glUniform1f(diffIntUniform, 0.7f);

	for(vec3 gvec : goombas){
		modelMat = glm::translate(mat4(1.0f), gvec+goombaOffset) * glm::rotate(mat4(1.0f), (float)-M_PI/2, vec3(0, 1, 0)) * glm::scale(mat4(1.0f), vec3(0.007f));
		glUniformMatrix4fv(modelUniform, 1, false, value_ptr(modelMat));
		glDrawElements(GL_TRIANGLES, goombaModel.numIndices, GL_UNSIGNED_INT, 0);
	}

	// Ground plane
	glBindVertexArray(planeVAO);
	glBindTexture(GL_TEXTURE_2D, floorTex);
	glUniform1i(texUniform, 1);
	glUniform1f(diffIntUniform, 1);
	glUniform1f(specIntUniform, 0);
	modelMat = glm::translate(mat4(1.0f), vec3(200, 0.885, 0)) * glm::scale(mat4(1.0f), vec3(250, 1, 50));
	glUniformMatrix4fv(modelUniform, 1, false, value_ptr(modelMat));
	glDrawArrays(GL_TRIANGLES, 0, 6);

	// Holes
	glBindVertexArray(cubeVAO);
	glUniform1i(texUniform, 0);
	glUniform3f(diffUniform, 0, 0, 0);
	glUniform1f(diffIntUniform, 0);
	glUniform1f(specIntUniform, 0);
	for(float h : holes){
		modelMat = glm::translate(mat4(1.0f), vec3(h, 0.885f, 0)) * glm::scale(mat4(1.0f), vec3(2, 0.1, 100));
		glUniformMatrix4fv(modelUniform, 1, false, value_ptr(modelMat));
		glDrawArrays(GL_TRIANGLES, 0, 36);
	}
}

GLuint GLWidget::loadShaders(const char* vertf, const char* fragf) {
	GLuint program = glCreateProgram();

	// read vertex shader from Qt resource file
	QFile vertFile(vertf);
	vertFile.open(QFile::ReadOnly | QFile::Text);
	QString vertString;
	QTextStream vertStream(&vertFile);
	vertString.append(vertStream.readAll());
	std::string vertSTLString = vertString.toStdString();

	const GLchar* vertSource = vertSTLString.c_str();

	GLuint vertShader = glCreateShader(GL_VERTEX_SHADER);
	glShaderSource(vertShader, 1, &vertSource, NULL);
	glCompileShader(vertShader);
	{
		GLint compiled;
		glGetShaderiv( vertShader, GL_COMPILE_STATUS, &compiled );
		if ( !compiled ) {
			GLsizei len;
			glGetShaderiv( vertShader, GL_INFO_LOG_LENGTH, &len );

			GLchar* log = new GLchar[len+1];
			glGetShaderInfoLog( vertShader, len, &len, log );
			std::cout << "Shader compilation failed: " << log << std::endl;
			delete [] log;
		}
	}
	glAttachShader(program, vertShader);

	// read fragment shader from Qt resource file
	QFile fragFile(fragf);
	fragFile.open(QFile::ReadOnly | QFile::Text);
	QString fragString;
	QTextStream fragStream(&fragFile);
	fragString.append(fragStream.readAll());
	std::string fragSTLString = fragString.toStdString();

	const GLchar* fragSource = fragSTLString.c_str();

	GLuint fragShader = glCreateShader(GL_FRAGMENT_SHADER);
	glShaderSource(fragShader, 1, &fragSource, NULL);
	glCompileShader(fragShader);
	{
		GLint compiled;
		glGetShaderiv( fragShader, GL_COMPILE_STATUS, &compiled );
		if ( !compiled ) {
			GLsizei len;
			glGetShaderiv( fragShader, GL_INFO_LOG_LENGTH, &len );

			GLchar* log = new GLchar[len+1];
			glGetShaderInfoLog( fragShader, len, &len, log );
			std::cerr << "Shader compilation failed: " << log << std::endl;
			delete [] log;
		}
	}
	glAttachShader(program, fragShader);

	glLinkProgram(program);
	{
		GLint linked;
		glGetProgramiv( program, GL_LINK_STATUS, &linked );
		if ( !linked ) {
			GLsizei len;
			glGetProgramiv( program, GL_INFO_LOG_LENGTH, &len );

			GLchar* log = new GLchar[len+1];
			glGetProgramInfoLog( program, len, &len, log );
			std::cout << "Shader linker failed: " << log << std::endl;
			delete [] log;
		}
	}

	return program;
}

GLuint GLWidget::loadTexture(string filename){
	QImage img(filename.c_str());

	if(img.isNull()){
		cout << "File not found: " << filename << endl;
		return 0;
	}

	if(img.format() != QImage::Format_RGBA8888){
		img = img.convertToFormat(QImage::Format_RGBA8888);
	}

	GLuint tex;
	glGenTextures(1, &tex);
	glBindTexture(GL_TEXTURE_2D, tex);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, img.width(), img.height(), 0, GL_RGBA, GL_UNSIGNED_BYTE, img.constBits());
	glGenerateMipmap(GL_TEXTURE_2D);

	return tex;
}

Model GLWidget::loadOBJ(const char* filename){
	vector<tinyobj::shape_t> shapes;
	vector<tinyobj::material_t> materials;
	QFile file(filename);
	string err = tinyobj::LoadObj(shapes, materials, file);

    if(!err.empty()) {
        cerr << err << endl;
        exit(1);
    }

    cout << "# of shapes: " << shapes.size() << endl;
    cout << "# of materials: " << materials.size() << endl;

    GLuint vao;
    glGenVertexArrays(1, &vao);
	glBindVertexArray(vao);

    GLuint posBuffer;
	glGenBuffers(1, &posBuffer);
	glBindBuffer(GL_ARRAY_BUFFER, posBuffer);
	glBufferData(GL_ARRAY_BUFFER, shapes[0].mesh.positions.size()*sizeof(float), &(shapes[0].mesh.positions[0]), GL_STATIC_DRAW);
	GLint posIndex = glGetAttribLocation(program, "position");
	glEnableVertexAttribArray(posIndex);
	glVertexAttribPointer(posIndex, 3, GL_FLOAT, GL_FALSE, 0, 0);

	GLuint normBuffer;
	glGenBuffers(1, &normBuffer);
	glBindBuffer(GL_ARRAY_BUFFER, normBuffer);
	glBufferData(GL_ARRAY_BUFFER, shapes[0].mesh.normals.size()*sizeof(float), &(shapes[0].mesh.normals[0]), GL_STATIC_DRAW);
	GLint normIndex = glGetAttribLocation(program, "normal");
	glEnableVertexAttribArray(normIndex);
	glVertexAttribPointer(normIndex, 3, GL_FLOAT, GL_FALSE, 0, 0);

	GLuint uvBuffer;
	glGenBuffers(1, &uvBuffer);
	glBindBuffer(GL_ARRAY_BUFFER, uvBuffer);
	glBufferData(GL_ARRAY_BUFFER, shapes[0].mesh.texcoords.size()*sizeof(float), &(shapes[0].mesh.texcoords[0]), GL_STATIC_DRAW);
	GLint uvIndex = glGetAttribLocation(program, "uv");
	glEnableVertexAttribArray(uvIndex);
	glVertexAttribPointer(uvIndex, 2, GL_FLOAT, GL_FALSE, 0, 0);

	GLuint indexBuffer;
	glGenBuffers(1, &indexBuffer);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indexBuffer);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, shapes[0].mesh.indices.size()*sizeof(unsigned int), &(shapes[0].mesh.indices[0]), GL_STATIC_DRAW);

	Model model;
	model.name = shapes[0].name;
	model.vao = vao;
	model.numIndices = shapes[0].mesh.indices.size();

	return model;
}

void GLWidget::keyPressEvent(QKeyEvent *event) {
	switch(event->key()) {
		case Qt::Key_W:
			// forward
			forward = true;
			break;
		case Qt::Key_A:
			// left
			left = true;
			break;
		case Qt::Key_S:
			// back
			back = true;
			break;
		case Qt::Key_D:
			// right
			right = true;
			break;
		case Qt::Key_Tab:
			// toggle fly mode
			flyMode = !flyMode;
			break;
		case Qt::Key_Shift:
			// down
			down = true;
			break;
		case Qt::Key_Space:
			// up or jump
			up = true;
			break;
		case Qt::Key_Escape:
			releaseMouse();
			setMouseTracking(false);
			setCursor(Qt::ArrowCursor);
			break;
	}
}

void GLWidget::keyReleaseEvent(QKeyEvent *event) {
	switch(event->key()) {
		case Qt::Key_W:
			// forward
			forward = false;
			break;
		case Qt::Key_A:
			// left
			left = false;
			break;
		case Qt::Key_S:
			// back
			back = false;
			break;
		case Qt::Key_D:
			// right
			right = false;
			break;
		case Qt::Key_Shift:
			// down
			down = false;
			break;
		case Qt::Key_Space:
			// up or jump
			up = false;
			break;
	}
}

void GLWidget::mousePressEvent(QMouseEvent *event) {
	// vec2 pt(event->x(), event->y());
	// lastPt = pt;
	grabMouse();
	setMouseTracking(true);
	setCursor(Qt::BlankCursor);
}

void GLWidget::mouseMoveEvent(QMouseEvent *event) {
	vec2 pt(event->globalX(), event->globalY());
		
	vec2 d = pt-center;

	yaw -= 0.005f*d.x;
	if(yaw > 2*M_PI) yaw -= 2*M_PI;
	else if(yaw < 0) yaw += 2*M_PI;
	pitch -= 0.005f*d.y;
	if(pitch > M_PI/2) pitch = M_PI/2;
	else if(pitch < -M_PI/2) pitch = -M_PI/2;
	
	yawMat = glm::rotate(mat4(1.0f), yaw, vec3(0, 1, 0));
	pitchMat = glm::rotate(mat4(1.0f), pitch, vec3(1, 0, 0));
	
	lastPt = pt;
}

void GLWidget::animate(){
	velocity.x = 0;
	velocity.z = 0;
	if(flyMode) velocity.y = 0;

	vec3 rightVec = vec3(yawMat[0][0], yawMat[0][1], yawMat[0][2]);
	vec3 fwdVec = -vec3(yawMat[2][0], yawMat[2][1], yawMat[2][2]);
	vec3 upVec = vec3(0, 1, 0);
	mat4 yp = yawMat * pitchMat;
	if(flyMode){
		rightVec = vec3(yp[0][0], yp[0][1], yp[0][2]);
		fwdVec = -vec3(yp[2][0], yp[2][1], yp[2][2]);
	}
	
	if(forward) velocity += fwdVec;
	if(back) velocity -= fwdVec;
	if(left) velocity -= rightVec;
	if(right) velocity += rightVec;
	if(up && (flyMode || position.y < 2)) velocity += upVec;
	if(down && flyMode) velocity -= upVec;

	position += 10 * 0.016f * velocity;

	if(!flyMode){
		if(position.y > 1.801)
			velocity.y -= 0.016*9.81;
		else if(position.y < 1.801){
			position.y = 1.8;
			velocity.y = 0;
		}
	}

	mat4 transMat = glm::translate(mat4(1.0f), position);
	
	viewMat = inverse(transMat * yp);
	
	if(hasMouseTracking())
		QCursor::setPos(center.x, center.y);


	if(goombaTime < 180)
		goombaOffset.x += 0.016;
	else
		goombaOffset.x -= 0.016;

	if((goombaTime/20) % 4 == 0 || (goombaTime/20) % 4 == 3)
		goombaOffset.z += 0.024;
	else
		goombaOffset.z -= 0.024;
	
	goombaTime += 1;
	if(goombaTime == 360) goombaTime = 0;

	update();
}
