#include "brickwall.h"

#ifndef M_PI
	#define M_PI 3.14159265358979323846
#endif

using namespace std;

BrickWall::BrickWall(vec3 position, float rotation, int width, int height, vec3 dims, float spacing){
	this->position = position;
	this->rotation = rotation;
	this->width = width;
	this->height = height;
	this->dims = dims;
	this->spacing = spacing;
}
BrickWall::~BrickWall(){}

void BrickWall::render(QOpenGLFunctions_3_3_Core* gl, GLuint cubeVAO, GLuint program){
	gl->glUseProgram(program);
	gl->glBindVertexArray(cubeVAO);

	GLint modelUniform = gl->glGetUniformLocation(program, "model");

	mat4 wallMat = glm::translate(mat4(1.0f), position) * glm::rotate(mat4(1.0f), rotation, vec3(0, 1, 0));

	for(int y = 0; y < height; y++){
		float yf = y*(dims.y + spacing);
		for(int x = 0; x < width; x++){
			float xf;
			if(y % 2 == 0)
				xf = x*(dims.x + spacing) - (width*(dims.x + spacing) - spacing)/2;
			else
				xf = (x+0.5f)*(dims.x + spacing) - (width*(dims.x + spacing) - spacing)/2;

			mat4 modelMat = wallMat * glm::translate(mat4(1.0f), vec3(xf, yf, 0)) * glm::scale(mat4(1.0f), dims);
			gl->glUniformMatrix4fv(modelUniform, 1, false, value_ptr(modelMat));

			gl->glDrawArrays(GL_TRIANGLES, 0, 36);
		}
	}
}