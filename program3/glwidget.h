#ifndef __GLWIDGET__INCLUDE__
#define __GLWIDGET__INCLUDE__

#include <string>

#include <QOpenGLWidget>
#include <QOpenGLFunctions_3_3_Core>
#include <QMouseEvent>
#include <QTimer>
#include <QCursor>
#include <QImage>
#include <QOpenGLTexture>

#define GLM_FORCE_RADIANS

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include <tinyobjloader/tiny_obj_loader.h>

#include "brickwall.h"

using glm::inverse;
using glm::vec2;
using glm::vec3;
using glm::vec4;
using glm::mat4;
using glm::perspective;
using glm::normalize;
using glm::length;
using glm::cross;
using glm::dot;
using glm::rotate;
using glm::value_ptr;
using glm::lookAt;

using std::string;
using std::vector;

class Model{
public:
	string name;
	GLuint vao;
	GLuint texture;
	int numIndices;
};

class GLWidget : public QOpenGLWidget, protected QOpenGLFunctions_3_3_Core {
	Q_OBJECT
public:
	GLWidget(QWidget *parent=0);
	~GLWidget();

protected:
	void initializeGL();
	void resizeGL(int w, int h);
	void paintGL();

	void mousePressEvent(QMouseEvent *event);
	void mouseMoveEvent(QMouseEvent *event);
	void keyPressEvent(QKeyEvent *event);
	void keyReleaseEvent(QKeyEvent *event);
	void moveEvent(QMoveEvent* event);

private:
	GLuint loadShaders(const char* vertf, const char* fragf);
	GLuint loadTexture(string filename);
	Model loadOBJ(const char* filename);

	void placeBrickBlock(int x, int y);
	void placeQuestionBlock(int x, int y);
	void placeHardBlock(int x, int y);
	void placeGoomba(int x, int y);
	void placePipe(int x, int height);
	void placeHole(int x);

	void animate();

	GLuint cubeVAO;
	GLuint planeVAO;
	Model goombaModel;
	Model pipeModel;

	GLuint program;

	GLuint questionTex;
	GLuint hardTex;
	GLuint floorTex;

	mat4 projMat;
	mat4 viewMat;

	float pitch;
	float yaw;
	mat4 pitchMat;
	mat4 yawMat;

	vec2 center;
	vec2 lastPt;

	QTimer timer;

	bool forward;
	bool back;
	bool left;
	bool right;
	bool up;
	bool down;
	bool flyMode;
	
	vec3 position;
	vec3 velocity;

	vector<BrickWall> walls;
	vector<vec2> question;
	vector<vec2> hardBlocks;
	vector<vec3> goombas;
	vector<vec2> pipes;
	vector<float> holes;

	int goombaTime;
	vec3 goombaOffset;
};

#endif