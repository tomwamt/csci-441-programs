# Virtual World

## Summary

For this project I chose to recreate World 1-1 from Super Mario Bros. in 3D. I 
created several methods to allow be to place blocks, objects, and enemies from 
the level. Due to difficult in modelling/creating these items, I was not able 
to recreate the flagpole or the castle at the end of the level.

The start of my 3D version:
![3d-start](http://i.imgur.com/bKDRsmN.png)

For reference, a map of the original level:
![map](http://www.mariowiki.com/images/e/e4/World_1-1_SMB.png)

## Details

*Implement a first person camera control.* You can use WASD to move around. 
Click on the screen to gain mouse control of the camera, and press Escape to 
regain the cursor. By deafult, you start in fly mode. Press Tab to switch 
between walk mode and fly mode.

*Light and shade your world using the Phong reflection model.* The bricks that 
make up brick blocks, the goombas, and the green pipes all use different values 
for diffuse color and specular intensity. In addition, the "black pits" were 
created by making a pitch black cube with no specular. I also used textured cubes 
to create several blocks in the world. I used a distant, highly positioned light 
source to simulate a sunny day. I also changed the clear color to the background 
color from the original game.

*Construct a virtual world.* I used several configurations of the brick wall object 
to create brick blocks. A large ground plane was rendered with a tiling texture. 
For extra object, I included basic cubes for question blocks, as well as a multitude 
of goombas patrolling the level.

*Animate at least one object in your world.* All of the goombas move back and forth 
along the level, forever on the lookout for Italian plumbers.

