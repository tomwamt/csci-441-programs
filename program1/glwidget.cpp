#include "glwidget.h"
#include <iostream>
#include <random>
#include <chrono>
#include <fstream>
#include <string>

#include <QTextStream>
#include <QColor>
#include <QOpenGLFramebufferObject>

using namespace std;

GLWidget::GLWidget(QWidget *parent) : QOpenGLWidget(parent), brushSize(5) {}
GLWidget::~GLWidget(){}

void GLWidget::initializeGL(){
	initializeOpenGLFunctions();

	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);

	program = loadShaders(":/vert.glsl", ":/frag.glsl");

	readBrushes("brushes.txt");

	for(int i = 0; i < 16; i++) proj[i] = 0;
	proj[0] = 2.0f/640;
	proj[5] = -2.0f/480;
	proj[10] = 1;
	proj[12] = -1;
	proj[13] = 1;
	proj[15] = 1;
}

void GLWidget::resizeGL(int w, int h){
	glViewport(0,0,w,h);

	float imageAspectRatio = (1.0f*image.width())/image.height();
	float windowAspectRatio = (1.0f*w)/h;
	if(imageAspectRatio > windowAspectRatio){
		scaleFactor = (1.0f*w)/image.width();
	}else{
		scaleFactor = (1.0f*h)/image.height();
	}

	offsetX = (width()-scaleFactor*image.width())/2;
	offsetY = (height()-scaleFactor*image.height())/2;

	proj[0] = scaleFactor * 2.0f/w;
	proj[5] = scaleFactor * -2.0f/h;
	proj[12] = -(scaleFactor*image.width())/w;
	proj[13] = (scaleFactor*image.height())/h;
}

void GLWidget::paintGL(){
	glClear(GL_COLOR_BUFFER_BIT);

	glUseProgram(program);
	GLint projUniform = glGetUniformLocation(program, "projection");
	glUniformMatrix4fv(projUniform, 1, GL_FALSE, proj);

	GLint modelUniform = glGetUniformLocation(program, "model");
	GLint colorUniform = glGetUniformLocation(program, "color");

	for(unsigned i = 0; i < shapes.size(); i++){
		Shape shape = shapes[i];
		Brush brush = brushes[shape.brushIndex];
		glBindVertexArray(brush.vao);
		glUniformMatrix4fv(modelUniform, 1, GL_FALSE, shape.model);
		glUniform3f(colorUniform, shape.r, shape.g, shape.b);
		glDrawArrays(GL_TRIANGLES, 0, brush.numVerts);
	}
	glBindVertexArray(0);
}

void GLWidget::mousePressEvent(QMouseEvent *event){
	int x = event->x();
	int y = event->y();

	createShape((x-offsetX)/scaleFactor, (y-offsetY)/scaleFactor);
	update();
}

void GLWidget::mouseMoveEvent(QMouseEvent *event){
	int x = event->x();
	int y = event->y();

	createShape((x-offsetX)/scaleFactor, (y-offsetY)/scaleFactor);
	update();
}

void GLWidget::createShape(int x, int y){
	if(x >= 0 && x < image.width() && y >= 0 && y < image.height()){
		QRgb rgb = image.pixel(x, y);
		int r = qRed(rgb);
		int g = qGreen(rgb);
		int b = qBlue(rgb);
		Shape shape;
		shape.r = r/255.0f;
		shape.g = g/255.0f;
		shape.b = b/255.0f;

		shape.x = x;
		shape.y = y;

		shape.brushIndex = currentBrush;
		shape.scale = brushSize;

		for(int i = 0; i < 16; i++) shape.model[i] = 0;
		shape.model[0] = shape.scale;
		shape.model[5] = shape.scale;
		shape.model[10] = 1;
		shape.model[12] = shape.x;
		shape.model[13] = shape.y;
		shape.model[15] = 1;

		shapes.push_back(shape);
	}
}

void GLWidget::keyPressEvent(QKeyEvent *event){
	switch(event->key()){
	case Qt::Key_Space:
		currentBrush = (currentBrush + 1) % brushes.size();
		brushChanged(currentBrush);
		cout << "Brush: " << brushes[currentBrush].name << endl;
		
		break;
	case Qt::Key_C:
		shapes.clear();
		cout << "Screen cleared." << endl;
		break;
	case Qt::Key_Comma:
	{
		int d = 1;
		if((event->modifiers() & Qt::ControlModifier) != 0)
			d = 10;
		brushSize -= d;
		if(brushSize < 1) brushSize = 1;
		brushSizeChanged(brushSize);
		cout << "Brush size: " << brushSize << endl;
		break;
	}
	case Qt::Key_Period:
	{
		int d = 1;
		if((event->modifiers() & Qt::ControlModifier) != 0)
			d = 10;
		brushSize += d;
		brushSizeChanged(brushSize);
		cout << "Brush size: " << brushSize << endl;
		break;
	}
	case Qt::Key_F:
	{
		unsigned timeSeed = chrono::system_clock::now().time_since_epoch().count();
		randomFill(50000/brushSize, timeSeed);
		cout << "Screen randomly filled." << endl;
		break;
	}
	case Qt::Key_G:
		if((event->modifiers() & Qt::ControlModifier) != 0)
			gridFill(true);
		else
			gridFill(false);
		cout << "Screen grid filled." << endl;
		break;
	case Qt::Key_Q:
		mosaicFill();
		cout << "Mosaic Done." << endl;
		break;
	case Qt::Key_S:
		if((event->modifiers() & Qt::ControlModifier) != 0){
			cout << "Saving screen as output.png... ";
			int w = width();
			int h = height();
			resize(image.width(), image.height());
			QOpenGLFramebufferObjectFormat format;
			format.setSamples(4);
			QOpenGLFramebufferObject fbo(image.width(), image.height(), format);
			fbo.bind();
			paintGL();
			fbo.release();
			QImage savedImage = fbo.toImage();
			savedImage.save("output.png");
			resize(w, h);
			cout << "Done." << endl;
		}
		break;
	case Qt::Key_O:
		if((event->modifiers() & Qt::ControlModifier) != 0){
			cout << "Enter the filename to load: ";
			string filename;
			cin >> filename;

			QImage img(filename.c_str());

			if(img.isNull()){
				cout << "File not found: " << filename << endl;
			}else{
				shapes.clear();
				setSourceImage(img);
				cout << filename << " loaded." << endl;
			}
		}
	}
	update();
}

void GLWidget::setSourceImage(QImage image){
	this->image = image;
}

void GLWidget::readBrushes(string filename){
	ifstream brushFile(filename);
	if(brushFile.is_open()){
		while(!brushFile.eof()){
			Brush brush;
			brushFile >> brush.name;
			brushFile >> brush.numVerts;
			int n = brush.numVerts * 2;
			vector<float> verts;
			for(int i = 0; i < n; i++){
				float f;
				brushFile >> f;
				verts.push_back(f);
			}

			glGenVertexArrays(1, &brush.vao);
			glBindVertexArray(brush.vao);

			GLuint vertexBuffer;
			glGenBuffers(1, &vertexBuffer);
			glBindBuffer(GL_ARRAY_BUFFER, vertexBuffer);
			glBufferData(GL_ARRAY_BUFFER, sizeof(float) * verts.size(), verts.data(), GL_STATIC_DRAW);
			GLint positionIndex = glGetAttribLocation(program, "position");
			glEnableVertexAttribArray(positionIndex);
			glVertexAttribPointer(positionIndex, 2, GL_FLOAT, GL_FALSE, 0, 0);

			if(brush.name.compare("") != 0)
				brushes.push_back(brush);
		}
		brushFile.close();
		currentBrush = 0;
	}else{
		cout << "Cannot open brush file " << filename << endl;
	}
}

void GLWidget::gridFill(bool randomBrush){
	unsigned timeSeed = chrono::system_clock::now().time_since_epoch().count();
	default_random_engine gen(timeSeed);
	uniform_int_distribution<int> dist(0, brushes.size()-1);

	int x = brushSize/2;
	int y = brushSize/2;
	while(x < image.width()){
		while(y < image.height()){
			if(randomBrush)
				currentBrush = dist(gen);
			createShape(x, y);
			y += brushSize;
		}
		x += brushSize;
		y = brushSize/2;
	}
}

void GLWidget::randomFill(int n, int seed){
	default_random_engine gen(seed);
	uniform_int_distribution<int> xdist(0, image.width()-1);
	uniform_int_distribution<int> ydist(0, image.height()-1);
	for(int i = 0; i < n; i++){
		int x = xdist(gen);
		int y = ydist(gen);

		createShape(x, y);
	}
}

void GLWidget::mosaicFill(){
	unsigned timeSeed = chrono::system_clock::now().time_since_epoch().count();
	for(int i = 200; i > 0; i /= 2){
		brushSize = i;
		int n = (image.width()*image.height())/(25*brushSize);
		randomFill(n, timeSeed);
	}
}

GLuint GLWidget::loadShaders(const char* vertf, const char* fragf) {
	GLuint program = glCreateProgram();

	// read vertex shader from Qt resource file
	QFile vertFile(vertf);
	vertFile.open(QFile::ReadOnly | QFile::Text);
	QString vertString;
	QTextStream vertStream(&vertFile);
	vertString.append(vertStream.readAll());
	std::string vertSTLString = vertString.toStdString();

	const GLchar* vertSource = vertSTLString.c_str();

	GLuint vertShader = glCreateShader(GL_VERTEX_SHADER);
	glShaderSource(vertShader, 1, &vertSource, NULL);
	glCompileShader(vertShader);
	{
		GLint compiled;
		glGetShaderiv( vertShader, GL_COMPILE_STATUS, &compiled );
		if ( !compiled ) {
			GLsizei len;
			glGetShaderiv( vertShader, GL_INFO_LOG_LENGTH, &len );

			GLchar* log = new GLchar[len+1];
			glGetShaderInfoLog( vertShader, len, &len, log );
			std::cout << "Shader compilation failed: " << log << std::endl;
			delete [] log;
		}
	}
	glAttachShader(program, vertShader);

	// read fragment shader from Qt resource file
	QFile fragFile(fragf);
	fragFile.open(QFile::ReadOnly | QFile::Text);
	QString fragString;
	QTextStream fragStream(&fragFile);
	fragString.append(fragStream.readAll());
	std::string fragSTLString = fragString.toStdString();

	const GLchar* fragSource = fragSTLString.c_str();

	GLuint fragShader = glCreateShader(GL_FRAGMENT_SHADER);
	glShaderSource(fragShader, 1, &fragSource, NULL);
	glCompileShader(fragShader);
	{
		GLint compiled;
		glGetShaderiv( fragShader, GL_COMPILE_STATUS, &compiled );
		if ( !compiled ) {
			GLsizei len;
			glGetShaderiv( fragShader, GL_INFO_LOG_LENGTH, &len );

			GLchar* log = new GLchar[len+1];
			glGetShaderInfoLog( fragShader, len, &len, log );
			std::cerr << "Shader compilation failed: " << log << std::endl;
			delete [] log;
		}
	}
	glAttachShader(program, fragShader);

	glLinkProgram(program);
	{
		GLint linked;
		glGetProgramiv( program, GL_LINK_STATUS, &linked );
		if ( !linked ) {
			GLsizei len;
			glGetProgramiv( program, GL_INFO_LOG_LENGTH, &len );

			GLchar* log = new GLchar[len+1];
			glGetProgramInfoLog( program, len, &len, log );
			std::cout << "Shader linker failed: " << log << std::endl;
			delete [] log;
		}
	}

	return program;
}

string GLWidget::getBrushName(int i){
	return brushes[i].name;
}

int GLWidget::numBrushes(){
	return brushes.size();
}

void GLWidget::gridFillButton(){
	gridFill(false);
	update();
}

void GLWidget::brushSizeBox(int size){
	brushSize = size;
}

void GLWidget::brushBox(int i){
	currentBrush = i;
}

void GLWidget::randomFillButton(){
	unsigned timeSeed = chrono::system_clock::now().time_since_epoch().count();
	randomFill(50000/brushSize, timeSeed);
	update();
}

void GLWidget::mosaicFillButton(){
	mosaicFill();
	update();
	brushSizeChanged(brushSize);
}

void GLWidget::clearButton(){
	shapes.clear();
	update();
}