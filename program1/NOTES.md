Program 1
---------

Grade Breakdown
---------------

    Position - 20/20
    Color    - 15/15
    Size     - 10/10
    Shape    - 10/10 
    Fill     - 15/15 
    Resize   - 10/10
    General  - 20/20
    -------------------------------
              100/100

Notes
-----
1. GUI. +2
2. Extra fill actions. +2
3. Extra shapes. +2
4. Extra sizes. +2
5. Fit image to window size. +2
6. Brushes input file
7. Saving/Opening
8. I'm not awarding extra credit, but you certainly went above and beyond. Great job!
