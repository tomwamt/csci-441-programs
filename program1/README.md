# NPR Paint Program

## Functionality
**Image input**: The image can be loaded in 2 ways, either as a command line argument to the program, or in a console prompt if no command line arguments are given.

**Drawing**: Clicking with any mouse button and dragging will cause shapes to be drawn on the screen. The color of the shape is determined by the corresponding pixel in the loaded image.

**Brushes**: A brush is a collection of triangles that make up a shape to be drawn. Brush shapes are loaded dynamically from a file named "brushes.txt" placed next to the program executable. The brush file consists of one or more entries, each having a name, a number of vertices, and a list of x-y coordinate pairs that define the vertices. The units used for vertices are pixels in the source image relative to where the user clicks, i.e. the center of the brush. For example, a centered 1x1 sqaure is defined as:
> `square
> 6
> -0.5 -0.5
> 0.5 -0.5
> -0.5 0.5
> -0.5 0.5
> 0.5 -0.5
> 0.5 0.5`

I have already defined four brushes in brushes.txt: the required square and circle, as well as a star and smiley face.

The size of the brush can be changed to any positive integer value. This scales the brush up to that size, in pixels of the source image.

**Fill actions**: There are 3 fill actions available:
* Grid Fill: Evenly fills the image in a grid pattern using the current brush shape and size.
* Random Fill: Randomly fills the screen with an even distribution using the current brush shape and size.
* Mosaic Fill: Repeatedly uses the Random Fill with varying brush sizes to create an artistic effect.

**Window Resizing**: A letterboxing effect is used in order to always view the entire image while maintaining the correct aspect ratio no matter the window size. All drawn are scaled to maintain their relative size with the image.

**Saving**: It is possible to save the current painting by pressing Ctrl-S. This saves an image to "output.png" that is the same size as the source image.

**Opening**: It is possible to load another image without restarting the program by pressing Crtl-O and typing the filename into the console.

## Sample Image
The sample image I chose is one that I found on the internet. I chose it because I thought it was a nice photograph, and I wanted to see what it would look like with my mosaic fill effect. I think the result is very interesting, and looks like the image was a reflection in water or through a rainy window.
![original](http://i.imgur.com/YJsMxZw.jpg)
![mosaic](http://i.imgur.com/KNjCRye.png)

## GUI
As an extra, I created a GUI using Qt for my program. Most program fuctions are accessable through this GUI, including fill actions, brush shape, and brush size. However, all key commands are functional as well, as long as the OpenGL widget has focus (by clicking on it).

## Key Commands
* **Space**: Cycle through brushes.
* **C**: Clear the screen.
* **Comma**: Decrease the brush size by 1. Hold Ctrl to decrease by 10 instead.
* **Period**: Increase the brush size by 1. Hold Ctrl to increase by 10 instead.
* **F**: Random fill using the current brush size and shape.
* **G**: Grid fill using the current brush size and shape. Hold Ctrl to use a random brush for each shape.
* **Q**: Mosaic fill using the current brush shape.
* **Ctrl-S**: Save the current drawing to "output.png"
* **Ctrl-O**: Open a new image. After pressing this, type a filename in the console.