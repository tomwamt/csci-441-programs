HEADERS += glwidget.h 
SOURCES += glwidget.cpp main.cpp

QT += opengl designer
CONFIG -= app_bundle
CONFIG += console c++11

QMAKE_CXXFLAGS += -DGL_GLEXT_PROTOTYPES

RESOURCES += shaders.qrc

FORMS += \
    mainwindow.ui