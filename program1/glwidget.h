#ifndef __GLWIDGET__INCLUDE__
#define __GLWIDGET__INCLUDE__

#include <QOpenGLWidget>
#include <QOpenGLFunctions_3_3_Core>
#include <QMouseEvent>
#include <QImage>

using std::vector;
using std::string;

class Shape{
public:
	float r, g, b;
	float x, y;
	float scale;
	float model[16];
	int brushIndex;
};

class Brush{
public:
	string name;
	GLuint vao;
	int numVerts;
};

class GLWidget : public QOpenGLWidget, protected QOpenGLFunctions_3_3_Core {
	Q_OBJECT

public:
	GLWidget(QWidget *parent=0);
	~GLWidget();

	void setSourceImage(QImage image);
	void readBrushes(string filename);
	int numBrushes();
	string getBrushName(int i);

protected:
	void initializeGL();
	void resizeGL(int w, int h);
	void paintGL();

	void mousePressEvent(QMouseEvent *event);
	void mouseMoveEvent(QMouseEvent *event);
	
	void keyPressEvent(QKeyEvent *event);

private:
	GLuint loadShaders(const char* vertf, const char* fragf);
	//static const GLchar* readShader(const char* filename);

	void createShape(int x, int y);
	void gridFill(bool randomBrush);
	void randomFill(int n, int seed=0);
	void mosaicFill();
	void paintFBO();

	QImage image;

	GLuint program;

	vector<Shape> shapes;

	vector<Brush> brushes;
	
	float proj[16];

	int currentBrush;
	int brushSize;

	float scaleFactor;
	float offsetX;
	float offsetY;

public slots:
	void brushSizeBox(int size);
	void gridFillButton();
	void randomFillButton();
	void mosaicFillButton();
	void clearButton();
	void brushBox(int i);

signals:
	void brushSizeChanged(int size);
	void brushChanged(int i);
};

#endif
