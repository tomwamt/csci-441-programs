#include <QApplication>
#include <iostream>
#include <string>

#include "ui_mainwindow.h"
#include "glwidget.h"

using namespace std;

int main(int argc, char** argv) {
	string filename;
	if(argc < 2){
		cout << "Enter the filename to load: ";
		cin >> filename;
	}else{
		filename = string(argv[1]);
	}

	QImage img(filename.c_str());

	if(img.isNull()){
		cout << "File not found: " << filename << endl;
		return 1;
	}

	QApplication a(argc, argv);

	QSurfaceFormat format;
	format.setVersion(3,3);
	format.setProfile(QSurfaceFormat::CoreProfile);
	format.setSamples(4);
	QSurfaceFormat::setDefaultFormat(format);

	QMainWindow *window = new QMainWindow;
    Ui::MainWindow ui;
    ui.setupUi(window);
    ui.glwidget->setSourceImage(img);

    window->show();

    for(int i = 0; i < ui.glwidget->numBrushes(); i++){
    	ui.comboBox->addItem(ui.glwidget->getBrushName(i).c_str());
    }
    ui.comboBox->setCurrentIndex(0);

	return a.exec();
}
